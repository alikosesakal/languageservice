package com.ali.ydsozluk.webservice;

import com.ali.ydsozluk.webservice.model.*;
import com.google.cloud.language.v1.*;
import org.json.JSONObject;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("/myresource")
public class MyResource extends HttpServlet{

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Path("simple")
    @Produces(MediaType.APPLICATION_JSON)
    public RootResponseObject  getIt() throws Exception {


        try (LanguageServiceClient language = LanguageServiceClient.create()){
            String text = "API keys are a simple encrypted string that can be used when" +
                    " calling certain APIs that don't need to access private user data.";

            Document document = Document.newBuilder().setContent(text)
                    .setType(Document.Type.PLAIN_TEXT).build();

            AnalyzeSyntaxRequest request = AnalyzeSyntaxRequest.newBuilder().setDocument(document)
                    .setEncodingType(EncodingType.UTF16)
                    .build();

            AnalyzeSyntaxResponse response = language.analyzeSyntax(request);


            return fillTokenList(response.getTokensList(),text);

        }

    }


    @GET
    @Path("/syntax")
    @Produces(MediaType.APPLICATION_JSON)
    public RootResponseObject analyzeSyntax(@QueryParam("pharagraph") String incomingText) throws Exception {

        try (LanguageServiceClient language = LanguageServiceClient.create()){

            Document document = Document.newBuilder().setContent(incomingText)
                    .setType(Document.Type.PLAIN_TEXT).build();

            AnalyzeSyntaxRequest request = AnalyzeSyntaxRequest.newBuilder().setDocument(document)
                    .setEncodingType(EncodingType.UTF16)
                    .build();

            AnalyzeSyntaxResponse response = language.analyzeSyntax(request);

            return fillTokenList(response.getTokensList(),incomingText);

        }
    }

    private RootResponseObject fillTokenList(List<Token> tokens, String incomingText){
        List<TokenModel> tokenModels = new ArrayList<>();
        for (Token token : tokens){
            TokenModel tokenModel = new TokenModel();

            DependencyOfEdgeModel dependency = new DependencyOfEdgeModel(
                    token.getDependencyEdge().getHeadTokenIndex(),token.getDependencyEdge().getLabel().name());
            TextModel textModel = new TextModel(token.getText().getContent(),token.getText().getBeginOffset());
            PartOfSpeecModel partOfSpeecModel = new PartOfSpeecModel(token.getPartOfSpeech().getTag().name(),
                    token.getPartOfSpeech().getNumber().name(),token.getPartOfSpeech().getVoice().name());

            tokenModel.setDependencyEdge(dependency);
            tokenModel.setText(textModel);
            tokenModel.setPartOfSpeech(partOfSpeecModel);
            tokenModel.setLemma(token.getLemma());
            tokenModels.add(tokenModel);
        }

        return new RootResponseObject(incomingText,tokenModels);
    }



}
