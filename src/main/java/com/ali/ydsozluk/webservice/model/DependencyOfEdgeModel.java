package com.ali.ydsozluk.webservice.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DependencyOfEdgeModel {
    private int headTokenIndex;
    private String label;

    public DependencyOfEdgeModel() {
        super();
    }

    public DependencyOfEdgeModel(int headTokenIndex, String label) {
        super();
        this.headTokenIndex = headTokenIndex;
        this.label = label;
    }

    public int getHeadTokenIndex() {
        return headTokenIndex;
    }

    public void setHeadTokenIndex(int headTokenIndex) {
        this.headTokenIndex = headTokenIndex;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
