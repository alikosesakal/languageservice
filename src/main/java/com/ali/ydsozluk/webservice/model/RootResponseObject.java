package com.ali.ydsozluk.webservice.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class RootResponseObject {
    private String text;
    private List<TokenModel> tokenModels;

    public RootResponseObject() {
        super();
    }

    public RootResponseObject(String text, List<TokenModel> tokenModels) {
        super();
        this.text = text;
        this.tokenModels = tokenModels;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<TokenModel> getTokenModels() {
        return tokenModels;
    }

    public void setTokenModels(List<TokenModel> tokenModels) {
        this.tokenModels = tokenModels;
    }
}
