package com.ali.ydsozluk.webservice.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TextModel {
    private String content;
    private int beginOfset;

    public TextModel(){
        super();
    }

    public TextModel(String content, int beginOfset){
        super();
        this.content = content;
        this.beginOfset = beginOfset;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getBeginOfset() {
        return beginOfset;
    }

    public void setBeginOfset(int beginOfset) {
        this.beginOfset = beginOfset;
    }
}
