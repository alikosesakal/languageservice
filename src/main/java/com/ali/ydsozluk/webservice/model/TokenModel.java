package com.ali.ydsozluk.webservice.model;


import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TokenModel {
    private TextModel text;
    private String lemma;
    private PartOfSpeecModel partOfSpeech;
    private DependencyOfEdgeModel dependencyEdge;

    public TokenModel(){
        super();
    }

    public TokenModel(TextModel text, String lemma, PartOfSpeecModel partOfSpeech
            , DependencyOfEdgeModel dependencyEdge){
        super();
        this.text = text;
        this.lemma = lemma;
        this.partOfSpeech = partOfSpeech;
        this.dependencyEdge = dependencyEdge;
    }

    public TextModel getText() {
        return text;
    }

    public void setText(TextModel text) {
        this.text = text;
    }

    public String getLemma() {
        return lemma;
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    public PartOfSpeecModel getPartOfSpeech() {
        return partOfSpeech;
    }

    public void setPartOfSpeech(PartOfSpeecModel partOfSpeech) {
        this.partOfSpeech = partOfSpeech;
    }

    public DependencyOfEdgeModel getDependencyEdge() {
        return dependencyEdge;
    }

    public void setDependencyEdge(DependencyOfEdgeModel dependencyEdge) {
        this.dependencyEdge = dependencyEdge;
    }
}
